<?php
function sanitizer($text)
{
    return preg_replace("/[0-9]{4} [0-9]{4} [0-9]{4} [0-9]{4}/","<removed>",$text);
}

$text = "Hello my credit card number is 1234 5678 0000 0000 \n";
$output = sanitizer($text);
echo $output;

$text = "Hello my phone number is +375 (33) 333 33 33 \n";
$output = sanitizer($text);
echo $output;

