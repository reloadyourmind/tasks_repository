<?php

interface Human extends HomoSapiens
{
    public function gender();
    public function name();
}

interface HomoSapiens 
{
    
}

abstract class Men implements Human
{
    public function gender()
    {
        echo "Men\n";
    }
}

class John extends Men
{
    public function name()
    {
        echo "Jonh\n";
    }
}

$john = new John;
$john->gender();
$john->name();

var_dump($john instanceof HomoSapiens);
